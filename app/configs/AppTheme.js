export default class AppTheme {
  static primaryColor = '#0388fc';
  static primaryFont = 'SFProText-Bold';
  static primaryFontSize = 17;
}
