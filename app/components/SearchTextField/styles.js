import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    borderRadius: 15,
  },

  textInput: {
    flex: 7,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
  },

  searchIcon: {
    flex: 1,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10,
    alignSelf: 'center',
  },
});

export default styles;
