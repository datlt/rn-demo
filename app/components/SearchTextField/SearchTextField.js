import React, {Component} from 'react';
import {View, TextInput, Image} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export default class SearchTextField extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[this.props.style, styles.container]}>
        <TextInput style={styles.textInput} />
        <Image
          style={styles.searchIcon}
          source={require('../../../assets/images/search_icons/search_icon.png')}
          resizeMode="contain"
        />
      </View>
    );
  }
}

SearchTextField.propTypes = {
  style: PropTypes.object,
};

SearchTextField.defaultProps = {
  style: {},
};
