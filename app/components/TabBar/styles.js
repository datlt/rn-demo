import {StyleSheet, Dimensions} from 'react-native';

const width = Dimensions.get('window').width / 5 - 3;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  tabBarItems: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#1e1e21',
    marginTop: 30,
  },

  tabBarItem: {
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
  },

  tabBarItem_text: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 10,
  },

  overlayView: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'center',
    top: 0,
    bottom: 0,
    alignSelf: 'center',
    width: width,
    height: width,
  },

  overlayView_background: {
    position: 'absolute',
    width: width,
    height: width,
    borderRadius: width / 2,
  },

  overlayView_center: {
    width: width,
    height: width,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
