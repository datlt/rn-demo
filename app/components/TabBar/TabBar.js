import React, {Component} from 'react';
import {View, Text, Image, TouchableWithoutFeedback} from 'react-native';
import styles from './styles';

export default class TabBar extends Component {
  constructor(props) {
    super(props);
  }

  tabBarImageAt(index) {
    if (index === 0) {
      return require('../../../assets/images/list_icons/list_icon.png');
    }

    if (index === 1) {
      return require('../../../assets/images/user_icons/user_icon.png');
    }

    if (index === 3) {
      return require('../../../assets/images/notification_icons/notification_icon.png');
    }

    return require('../../../assets/images/contact_icons/contact_icon.png');
  }

  tabBarTitleAt(index) {
    if (index === 0) {
      return '???';
    }

    if (index === 1) {
      return 'ACCOUNT';
    }

    if (index === 3) {
      return 'NOTIFICATIONS';
    }

    return 'CONTACT';
  }

  tabBarItemSelectedAt(index) {
    if (this.props.onSelected !== undefined) {
      this.props.onSelected(index);
    }
  }

  tabBarItemView(index) {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.tabBarItemSelectedAt(index)}>
        <View style={styles.tabBarItem}>
          <Image resizeMode="contain" source={this.tabBarImageAt(index)} />
          <Text style={styles.tabBarItem_text}>
            {this.tabBarTitleAt(index)}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.tabBarItems}>
          {this.tabBarItemView(0)}
          {this.tabBarItemView(1)}
          <View style={styles.tabBarItem} />
          {this.tabBarItemView(3)}
          {this.tabBarItemView(4)}
        </View>
        <TouchableWithoutFeedback onPress={() => this.tabBarItemSelectedAt(2)}>
          <View style={styles.overlayView}>
            <Image
              style={styles.overlayView_background}
              resizeMode="stretch"
              source={require('../../../assets/images/login_background.png')}
            />
            <View style={styles.overlayView_center}>
              <Image
                resizeMode="contain"
                source={require('../../../assets/images/list2_icons/list2_icon.png')}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
