import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  container: {
    flex: 1,
  },

  content: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 30,
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 10,
    backgroundColor: 'white',
  },

  listView: {
    flex: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 50,
    marginBottom: 50,
  },

  listViewItem: {
    justifyContent: 'center',
    height: 70,
  },

  listViewItem_divider: {
    height: 1,
    backgroundColor: 'grey',
  },

  optionsView: {
    justifyContent: 'flex-end',
    marginRight: 20,
    marginTop: 50,
    marginBottom: 50,
    padding: 10,
  },

  optionsView_item: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: 7,
    backgroundColor: '#ededed',
  },

  optionsView_item_image: {
    width: 30,
    height: 30,
  },

  optionsView_item_top: {
    position: 'absolute',
    top: 0,
    alignSelf: 'center',
  },
});

export default styles;
