import React, {Component} from 'react';
import {View, Image, Text, SafeAreaView, FlatList} from 'react-native';
import styles from './styles';

export default class TestScreen extends Component {
  constructor(props) {
    super(props);
  }

  optionItemAt(index) {
    const itemStyles =
      index === 0
        ? [styles.optionsView_item, styles.optionsView_item_top]
        : [styles.optionsView_item];
    return (
      <View style={itemStyles}>
        <Image
          style={styles.optionsView_item_image}
          resizeMode="contain"
          source={this.imageAt(index)}
        />
      </View>
    );
  }

  imageAt(index) {
    if (index === 0) {
      return require('../../../assets/images/power_icon.png');
    }

    if (index === 1) {
      return require('../../../assets/images/home_icon.png');
    }

    if (index === 2) {
      return require('../../../assets/images/setting_icon.png');
    }

    if (index === 3) {
      return require('../../../assets/images/more_icon.png');
    }

    return require('../../../assets/images/monitor_icon.png');
  }

  listItem(title) {
    return (
      <>
        <View style={styles.listViewItem}>
          <Text>{title}</Text>
        </View>
        <View style={styles.listViewItem_divider} />
      </>
    );
  }

  render() {
    const data = [
      'Home',
      'Live Flight Details',
      'Checked Baggage',
      'Hand Baggage',
      'Essential Information',
      'Booking Conditions',
      'Amendment & Cancellation',
      'Airport Hotel & Parking',
      'ATOL Certificate',
      'Visa & Passport',
      'Insurance Policy',
      'Manage My Booking',
    ];
    return (
      <>
        <Image
          style={styles.backgroundImage}
          resizeMode="stretch"
          source={require('../../../assets/images/backgrounds/background.png')}
        />
        <SafeAreaView style={styles.container}>
          <View style={styles.content}>
            <FlatList
              style={styles.listView}
              data={data}
              renderItem={({item}) => this.listItem(item)}
              keyExtractor={(item, index) => item}
              showsVerticalScrollIndicator={false}
            />
            <View style={styles.optionsView}>
              {this.optionItemAt(0)}
              {this.optionItemAt(1)}
              {this.optionItemAt(2)}
              {this.optionItemAt(3)}
              {this.optionItemAt(4)}
            </View>
          </View>
        </SafeAreaView>
      </>
    );
  }
}
