import {StyleSheet} from 'react-native';
import AppTheme from '../../../configs/AppTheme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  form: {
    backgroundColor: '#ffffff',
    alignItems: 'stretch',
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30,
    paddingTop: 30,
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 15,
  },

  form_input_container: {
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 20,
    marginTop: 10,
  },

  form_input: {
    height: 40,
    marginLeft: 15,
    marginRight: 15,
    fontFamily: AppTheme.primaryFont,
  },

  form_checkbox: {
    flexDirection: 'row-reverse',
    marginTop: 30,
    marginBottom: 10,
  },

  form_checkbox_box: {
    width: 20,
    height: 20,
    borderColor: '#878787',
  },

  form_checkbox_text: {
    marginRight: 10,
    fontFamily: AppTheme.primaryFont,
  },

  loginButton: {
    alignSelf: 'center',
    justifyContent: 'center',
    bottom: 30,
    height: 60,
    width: 200,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
  },

  loginButton_background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    height: 60,
    width: 200,
    borderRadius: 30,
  },

  loginButton_text: {
    textAlign: 'center',
    fontFamily: AppTheme.primaryFont,
    color: '#ffffff',
  },
});

export default styles;
