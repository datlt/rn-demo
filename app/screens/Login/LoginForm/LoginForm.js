import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  CheckBox,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rerender: false,
      fullName: '',
      bookingNumber: '',
    };

    this.handleLoginButtonClick = this.handleLoginButtonClick.bind(this);
    this.onFullNameChange = this.onFullNameChange.bind(this);
    this.handleLoginButtonClick = this.handleLoginButtonClick.bind(this);
  }

  shouldComponentUpdate() {
    return this.setState.rerender;
  }

  handleLoginButtonClick() {
    if (this.props.onLogin === undefined) {
      return;
    }
    this.props.onLogin({
      fullName: this.state.fullName,
      bookingNumber: this.state.bookingNumber,
    });
  }

  onFullNameChange(text) {
    this.setState({
      fullName: text,
    });
  }

  onBookingNumberChange(text) {
    this.setState({
      bookingNumber: text,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.form}>
          <View style={styles.form_input_container}>
            <TextInput
              style={styles.form_input}
              placeholder="Passenger Full Name"
              onChangeText={text => this.onFullNameChange(text)}
            />
          </View>
          <View style={styles.form_input_container}>
            <TextInput
              ref={this.bookingNumber}
              style={styles.form_input}
              placeholder="Booking Number"
              onChangeText={text => this.onBookingNumberChange(text)}
            />
          </View>
          <View style={styles.form_checkbox}>
            <CheckBox value={false} style={styles.form_checkbox_box} />
            <Text style={styles.form_checkbox_text}>Remember me</Text>
          </View>
        </View>
        <TouchableWithoutFeedback onPress={this.handleLoginButtonClick}>
          <View style={styles.loginButton}>
            <Image
              style={styles.loginButton_background}
              resizeMode="stretch"
              source={require('../../../../assets/images/login_background.png')}
            />
            <Text style={styles.loginButton_text}>Login</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

LoginForm.prototypes = {
  onLogin: PropTypes.func,
};
