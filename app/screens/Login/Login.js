import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import styles from './styles';
import LoginForm from './LoginForm/LoginForm';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(values) {
    this.props.navigation.navigate('HomeTabs');
  }

  render() {
    return (
      <>
        <Image
          style={styles.backgroundImage}
          resizeMode="stretch"
          source={require('../../../assets/images/backgrounds/background.png')}
        />
        <View style={styles.container}>
          <View style={styles.topSection}>
            <View style={styles.topSection_view}>
              <Text style={styles.topSection_text}>B78</Text>
            </View>
          </View>
          <View style={styles.centerSection}>
            <LoginForm onLogin={this.handleLogin} />
          </View>
          <View style={styles.bottomSection} />
        </View>
      </>
    );
  }
}
