import {StyleSheet, Dimensions} from 'react-native';
import AppTheme from '../../configs/AppTheme';

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  container: {
    flex: 1,
  },

  topSection: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  topSection_view: {
    padding: 20,
  },

  topSection_text: {
    fontFamily: AppTheme.primaryFont,
    fontSize: AppTheme.primaryFontSize,
  },

  centerSection: {
    flex: 6,
  },

  bottomSection: {
    flex: 2,
  },
});

export default styles;
