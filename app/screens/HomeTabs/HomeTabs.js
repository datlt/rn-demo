import React, {Component} from 'react';
import Home from '../../screens/Home/Home';
import TestScreen from '../../screens/TestScreen/TestScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {TabActions} from '@react-navigation/native';
import styles from './styles';
import {View, Image} from 'react-native';
import {TabBar} from '../../components';

const Tab = createBottomTabNavigator();

export default class HomeTabs extends Component {
  constructor(props) {
    super(props);
  }

  handleTabBarSelectedAt(index) {
    if (index === 2) {
      const jumpToAction = TabActions.jumpTo('TestScreen');
      this.props.navigation.dispatch(jumpToAction);
    } else {
      const jumpToAction = TabActions.jumpTo('Home');
      this.props.navigation.dispatch(jumpToAction);
    }
  }

  render() {
    return (
      <>
        <Image
          style={styles.backgroundImage}
          resizeMode="stretch"
          source={require('../../../assets/images/backgrounds/background.png')}
        />
        <View style={styles.content}>
          <Tab.Navigator
            tabBarOptions={{
              showLabel: false,
              showIcon: false,
              style: styles.tabBar,
            }}>
            <Tab.Screen
              options={{tabBarVisible: false}}
              name="Home"
              component={Home}
            />
            <Tab.Screen
              options={{tabBarVisible: false}}
              name="TestScreen"
              component={TestScreen}
            />
          </Tab.Navigator>
        </View>
        <View style={styles.tabBar}>
          <TabBar onSelected={index => this.handleTabBarSelectedAt(index)} />
        </View>
        <View style={styles.bottomView} />
      </>
    );
  }
}
