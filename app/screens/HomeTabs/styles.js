import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  content: {
    flex: 8,
  },

  tabBar: {
    flex: 1,
    justifyContent: 'flex-start',
  },

  bottomView: {
    flex: 0.2,
    backgroundColor: '#1e1e21',
  },
});

export default styles;
