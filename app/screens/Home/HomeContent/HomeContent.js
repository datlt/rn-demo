import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';
import styles from './styles';

export default class HomeContent extends Component {
  constructor(props) {
    super(props);
    this.optionView = this.optionView.bind(this);
  }

  optionView(title) {
    return (
      <View style={styles.overview_options_container}>
        <Text style={styles.overview_options_container_text}>{title}</Text>
        <Image
          style={styles.overview_options_container_image}
          resizeMode="contain"
          source={require('../../../../assets/images/next_icons/next_icon.png')}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.overview}>
          <View style={styles.overview_userInfo}>
            <Text style={[styles.overview_userInfo_text]}>
              Aleksander Twardowski
            </Text>
            <Text
              style={[
                styles.overview_userInfo_text,
                styles.overview_userInfo_booking,
              ]}>
              Booking: 123
            </Text>
          </View>
          <View style={styles.overview_options}>
            {this.optionView('Current Booking')}
            {this.optionView('Previous Bookings')}
            {this.optionView('My Profile')}
            {this.optionView('Urgent Assisstance')}
          </View>
        </View>
        <View style={styles.avatarContainer}>
          <Image
            style={styles.avatarImage}
            resizeMode="contain"
            source={require('../../../../assets/images/user_icons/user_icon.png')}
          />
        </View>
      </View>
    );
  }
}
