import {StyleSheet} from 'react-native';
import AppTheme from '../../../configs/AppTheme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  overview: {
    marginTop: 80,
    borderRadius: 20,
    backgroundColor: 'white',
  },

  overview_userInfo: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 60,
  },

  overview_userInfo_text: {
    fontFamily: AppTheme.primaryFont,
    fontSize: AppTheme.primaryFontSize,
    textAlign: 'center',
  },

  overview_userInfo_booking: {
    fontSize: 14,
    color: 'grey',
    marginTop: 5,
  },

  overview_options: {
    justifyContent: 'flex-start',
    marginLeft: 40,
    marginRight: 40,
    marginBottom: 20,
    marginTop: 35,
  },

  overview_options_container: {
    backgroundColor: '#ededed',
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 3,
    marginBottom: 3,
  },

  overview_options_container_text: {
    flex: 10,
    marginLeft: 20,
    marginTop: 15,
    marginBottom: 15,
    fontFamily: AppTheme.primaryFont,
    fontSize: AppTheme.primaryFontSize,
  },

  overview_options_container_image: {
    flex: 1,
    marginRight: 10,
  },

  avatarContainer: {
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 4,
    borderColor: 'white',
    backgroundColor: 'grey',
    top: 25,
  },

  avatarImage: {
    alignSelf: 'center',
    width: 60,
    height: 60,
  },
});

export default styles;
