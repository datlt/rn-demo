import React, {Component} from 'react';
import {View, SafeAreaView, Image} from 'react-native';
import {TabActions} from '@react-navigation/native';
import {SearchTextField} from '../../components';
import HomeContent from './HomeContent/HomeContent';
import styles from './styles';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.handleTabBarSelectedAt = this.handleTabBarSelectedAt.bind(this);
  }

  handleTabBarSelectedAt(index) {
    if (index !== 2) {
      return;
    }

    const jumpToAction = TabActions.jumpTo('TestScreen');
    this.props.navigation.dispatch(jumpToAction);
  }

  render() {
    return (
      <>
        <Image
          style={styles.backgroundImage}
          resizeMode="stretch"
          source={require('../../../assets/images/backgrounds/background.png')}
        />
        <SafeAreaView style={styles.container}>
          <View style={styles.searchSection}>
            <SearchTextField />
          </View>
          <View style={styles.contentSection}>
            <HomeContent />
          </View>
        </SafeAreaView>
      </>
    );
  }
}
