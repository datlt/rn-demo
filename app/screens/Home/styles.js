import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  container: {
    flex: 1,
  },

  searchSection: {
    flex: 2,
    marginLeft: 20,
    marginRight: 20,
  },

  contentSection: {
    flex: 30,
    marginTop: 30,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
  },

  tabBarSection: {
    flex: 5,
  },
});

export default styles;
